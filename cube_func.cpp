#include <iostream>
#include <cmath>
using namespace std;

int cube(int x){
    return pow(x, 3);
}

int main() {
    int num, cub;
    cout << "Enter a number: ";
    cin >> num;
    cub = cube(num);

    cout << num << " to the power of 3 is " << cub;
}
